pyGtkRDP version 1.3

Simple gtk3 application for freerdp or rdesktop connection.

![pyGtkRDP](https://dev.ussr.win/oss-it/pygtkrdp/raw/branch/master/pygtkrdp.png)

Current command line adapted for nightly builds of FreeRDP ( https://github.com/FreeRDP/FreeRDP/wiki/PreBuilds ).
If you want to use rdesktop or another RDP client, change the source accordingly.